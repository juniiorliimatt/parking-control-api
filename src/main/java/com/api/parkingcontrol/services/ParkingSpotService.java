package com.api.parkingcontrol.services;

import com.api.parkingcontrol.models.ParkingSpotModel;
import com.api.parkingcontrol.repositories.ParkingSpotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */

@Service
public class ParkingSpotService{

  private final ParkingSpotRepository repository;

  @Autowired
  public ParkingSpotService(ParkingSpotRepository repository){
	this.repository = repository;
  }

  @Transactional(readOnly = true)
  public Page<ParkingSpotModel> findAll(Pageable pageable){
	return repository.findAll(pageable);
  }

  @Transactional(readOnly = true)
  public Optional<ParkingSpotModel> findById(UUID id){
	return repository.findById(id);
  }

  @Transactional()
  public ParkingSpotModel save(ParkingSpotModel parkingSpotModel){
	return repository.save(parkingSpotModel);
  }

  @Transactional()
  public ParkingSpotModel update(ParkingSpotModel parkingSpotModel){
	return repository.save(parkingSpotModel);
  }

  @Transactional()
  public void delete(UUID id){
	repository.deleteById(id);
  }

  public boolean existsByLicensePlateCar(String plateCar){
	return repository.existsByLicensePlateCar(plateCar);
  }

  public boolean existsByParkingSpotNumber(String number){
	return repository.existsByParkingSpotNumber(number);
  }

  public boolean existsByApartmentAndBlock(String apartment, String block){
	return repository.existsByApartmentAndBlock(apartment, block);
  }

}
