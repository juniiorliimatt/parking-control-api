package com.api.parkingcontrol.controllers.dtos;

import com.api.parkingcontrol.models.ParkingSpotModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */


@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingSpotDto{

  private UUID id;

  @NotBlank
  private String parkingSpotNumber;

  @NotBlank
  @Size(max = 7)
  private String licensePlateCar;

  @NotBlank
  private String brandCar;

  @NotBlank
  private String modelCar;

  @NotBlank
  private String colorCar;

  private LocalDateTime registrationDate;

  @NotBlank
  private String responsibleName;

  @NotBlank
  private String apartment;

  @NotBlank
  private String block;

  public static ParkingSpotDto convertToDto(ParkingSpotModel model){
	ModelMapper mapper = new ModelMapper();
	return mapper.map(model, ParkingSpotDto.class);
  }

  public static ParkingSpotModel convertToModel(ParkingSpotDto dto){
	ModelMapper mapper = new ModelMapper();
	return mapper.map(dto, ParkingSpotModel.class);
  }

}
