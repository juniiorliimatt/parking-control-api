package com.api.parkingcontrol.controllers.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */

@Getter
@SuperBuilder
@JsonInclude(NON_NULL)
public class Response{

  private LocalDateTime timeStamp;
  private int statusCode;
  private HttpStatus status;
  private String message;
  private Iterable<?> data;

}

