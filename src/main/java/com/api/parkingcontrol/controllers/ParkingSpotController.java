package com.api.parkingcontrol.controllers;

import com.api.parkingcontrol.controllers.dtos.ParkingSpotDto;
import com.api.parkingcontrol.controllers.response.Response;
import com.api.parkingcontrol.models.ParkingSpotModel;
import com.api.parkingcontrol.services.ParkingSpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/parking-spot")
public class ParkingSpotController{

  private final ParkingSpotService service;

  @Autowired
  public ParkingSpotController(ParkingSpotService service){
	this.service = service;
  }

  @GetMapping("/find/all")
  public ResponseEntity<Response> findAll(@PageableDefault(page = 0, size = 5, sort = "apartment", direction = Sort.Direction.ASC) Pageable pageable){
	Page<ParkingSpotModel> list = service.findAll(pageable);
	return ResponseEntity.status(HttpStatus.OK)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .message("Parking Spot List")
									   .data(list)
									   .build());
  }

  @GetMapping("/find/id/{id}")
  public ResponseEntity<Response> findById(@PathVariable(value = "id") UUID id){
	var parkingSpotModelOptional = service.findById(id);
	return getResponse(parkingSpotModelOptional);
  }


  @PostMapping("/save")
  public ResponseEntity<Response> save(@RequestBody @Valid ParkingSpotDto dto){

	if(service.existsByLicensePlateCar(dto.getLicensePlateCar())){
	  return existsByLicensePlateCar();
	}

	if(service.existsByParkingSpotNumber(dto.getParkingSpotNumber())){
	  return existsByParkingSpotNumber();
	}

	if(service.existsByApartmentAndBlock(dto.getApartment(), dto.getBlock())){
	  return existsByApartmentAndBlock();
	}

	var parkingSpotModel = ParkingSpotDto.convertToModel(dto);
	parkingSpotModel.setRegistrationDate(LocalDateTime.now(ZoneId.of("UTC")));
	var dtoResponse = ParkingSpotDto.convertToDto(service.save(parkingSpotModel));
	return ResponseEntity.status(HttpStatus.CREATED)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.CREATED.value())
									   .status(HttpStatus.CREATED)
									   .message("Parking Spot Created")
									   .data(List.of(dtoResponse))
									   .build());
  }

  @PutMapping("/update")
  public ResponseEntity<Response> update(@RequestBody @Valid ParkingSpotDto dto){
	var parkingExists = service.findById(dto.getId());
	if(parkingExists.isEmpty()){
	  return ResponseEntity.status(HttpStatus.NOT_FOUND)
						   .body(Response.builder()
										 .timeStamp(LocalDateTime.now())
										 .statusCode(HttpStatus.NOT_FOUND.value())
										 .status(HttpStatus.NOT_FOUND)
										 .message("Parking Spot not found ")
										 .build());
	}

	var parkingSpotModel = ParkingSpotDto.convertToModel(dto);
	var parkingSpotModelUpdated = service.update(parkingSpotModel);
	var dtoResponse = ParkingSpotDto.convertToDto(parkingSpotModelUpdated);
	return ResponseEntity.status(HttpStatus.OK)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .message("Parking Spot Updated")
									   .data(List.of(dtoResponse))
									   .build());
  }

  @DeleteMapping("/delete/id/{id}")
  public ResponseEntity<Response> delete(@PathVariable(value = "id") UUID id){
	Optional<ParkingSpotModel> parkingSpotModel = service.findById(id);
	if(parkingSpotModel.isEmpty()){
	  return ResponseEntity.status(HttpStatus.NOT_FOUND)
						   .body(Response.builder()
										 .timeStamp(LocalDateTime.now())
										 .statusCode(HttpStatus.NOT_FOUND.value())
										 .status(HttpStatus.NOT_FOUND)
										 .message("Parking Spot not found")
										 .build());
	}

	service.delete(id);
	return ResponseEntity.status(HttpStatus.OK)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .message("Parking Spot deleted successfully")
									   .build());
  }

  private ResponseEntity<Response> getResponse(Optional<ParkingSpotModel> parkingSpotModel){
	if(parkingSpotModel.isPresent()){
	  var dto = ParkingSpotDto.convertToDto(parkingSpotModel.get());
	  return ResponseEntity.status(HttpStatus.OK)
						   .body(Response.builder()
										 .timeStamp(LocalDateTime.now())
										 .statusCode(HttpStatus.OK.value())
										 .status(HttpStatus.OK)
										 .message("Parking Spot: " + dto.getParkingSpotNumber())
										 .data(List.of(dto))
										 .build());
	}

	return ResponseEntity.status(HttpStatus.NOT_FOUND)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.NOT_FOUND.value())
									   .status(HttpStatus.NOT_FOUND)
									   .message("Parking Spot not found ")
									   .build());
  }

  private ResponseEntity<Response> existsByLicensePlateCar(){
	return ResponseEntity.status(HttpStatus.CONFLICT)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.CONFLICT.value())
									   .status(HttpStatus.CONFLICT)
									   .message("Conflict: License Plate Car is already in use!")
									   .build());
  }

  private ResponseEntity<Response> existsByParkingSpotNumber(){
	return ResponseEntity.status(HttpStatus.CONFLICT)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.CONFLICT.value())
									   .status(HttpStatus.CONFLICT)
									   .message("Conflict: Parking Spot is already in use!")
									   .build());
  }

  private ResponseEntity<Response> existsByApartmentAndBlock(){
	return ResponseEntity.status(HttpStatus.CONFLICT)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.CONFLICT.value())
									   .status(HttpStatus.CONFLICT)
									   .message("Conflict: Parking Spot already registered for this apartment/block!")
									   .build());
  }

}
