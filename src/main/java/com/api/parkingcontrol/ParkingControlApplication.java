package com.api.parkingcontrol;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */

@SpringBootApplication
public class ParkingControlApplication{
  public static void main(String[] args){
	SpringApplication.run(ParkingControlApplication.class, args);
  }

}
