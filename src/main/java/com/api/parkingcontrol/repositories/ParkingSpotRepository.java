package com.api.parkingcontrol.repositories;

import com.api.parkingcontrol.controllers.dtos.ParkingSpotDto;
import com.api.parkingcontrol.models.ParkingSpotModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author Lima Junior
 * @version 0.0.1
 * @since 10/02/2022
 */

@Repository
public interface ParkingSpotRepository extends JpaRepository<ParkingSpotModel, UUID>{

  boolean existsByLicensePlateCar(String plateCar);
  boolean existsByParkingSpotNumber(String number);
  boolean existsByApartmentAndBlock(String apartment, String block);

}
